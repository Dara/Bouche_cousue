<?php
session_start()
?>

<!DOCTYPE html>
<html lang="fr">
<head>
<meta charset="UTF-8">
<title>Bouche cousue : le jeu</title>
<link rel="shortcut icon" type="image/x-icon" href="favicon.ico" />
</head>
<body>

<div style="display: block;width:50%;float:left">
<?php 
// récupère les variables de bouchecousue2
$mot=$_SESSION["mot"];
$longueur=strlen($mot);

$entete=$_POST["entete"];
$proposition=$_POST['proposition'];

// gère les accents de la proposition + la met en minuscule
$proposition=strtolower($proposition);
$proposition=enleve_accents($proposition);

function enleve_accents($texte) {
    return preg_replace('~&([a-z]{1,2})(?:acute|cedil|circ|grave|lig|orn|ring|slash|th|tilde|uml|caron);~i', '$1', htmlentities($texte, ENT_QUOTES, 'UTF-8'));
}

// Fonction de Lucien

//function normalize (str) {
//	$nfd = Normalizer::normalize(Normalizer::FORM_D);
//	$latin = preg_replace('![^a-zA-Z]!', '', $nfd);
//	return strtolower($latin);
//}

// Si la proposition n'est pas du bon nombre de lettres
if(strlen($proposition)<$longueur){
	echo "$entete"."<br />";
	echo 'Le mot est trop court !';
}

// Vérifie si le mot est dans le dico
$s="mots_".$longueur."_lettres.txt";
$handle=fopen($s,"r");

$verif=0;

if($handle){
	while(!feof($handle)){
		$osef=fgets($handle);
		$sousverif=0;
		for($k=0;$k<=$longueur-1;$k++){
			$letter=fgetc($handle);
			if(substr($proposition,$k,1)==$letter){
				$sousverif++;
			}
		}
		if($sousverif==$longueur){
			$verif=1;
		}
	}
	fclose($handle);
}

if($verif==0 && strlen($proposition)==$longueur){
	echo $entete."<br /><br />";
	echo "Le mot ''$proposition'' <a href='https://fr.wiktionary.org/wiki/Utilisateur:DaraDaraDara/bouchecousue'>n'est pas accepté</a> ou bien <a href='https://fr.wiktionary.org/w/index.php?title=Spécial:Recherche&search=$proposition'>n'est pas dans le Wiktionnaire</a> (mais vous pouvez l'y ajouter si cela vous semble légitime).<br /><br />";
}

//Boucle principale (donne le nombre de lettres en commun)
if(strlen($proposition)==$longueur && $verif==1){
	$commun=0;
	for ($i=0;$i<=$longueur-1;$i++){
		if(substr($mot,$i,1)==substr($proposition,$i,1)){
			$commun++;
		}
	}
	$_SESSION['coups']++;
	$entete=$entete."<br /><br />";
	
	for($k=0;$k<=$longueur-1;$k++){
		$entete=$entete."<DIV STYLE='float:left;border:solid 1px gray;width:1em;text-align:center;'>".substr($proposition,$k,1)."</DIV>";
	}
	$entete=$entete."<div style='float:left;margin-left:10px;padding-top:2px;'>$commun/$longueur</div>";
	echo $entete."<br /><br />";
}

// Si c'est gagné !
if ($proposition==$mot && strlen($proposition)==$longueur && $verif==1){
	$coups=$_SESSION['coups'];
	echo "Bravo, tu as gagné en $coups coups !<br /><a href='https://fr.wiktionary.org/w/index.php?title=Spécial:Recherche&search=$mot' target='_blank'>Voir la page Wiktionnaire du mot</a><br />";
	echo "<div style='text-align:center;'>Rejouer ?</div>";
	include('bouchecousue2bis_nbLettres.php');
	exit();
}
?>

<form method="post" action="bouchecousue3_alea.php" id="formform">

	<input type="text" name="proposition" maxlength="<?php echo $longueur; ?>" size="<?php echo $longueur; ?>" autocomplete="off" autofocus>
	<input type="submit" />

	<div style="visibility: hidden">
		<input name="entete" value="<?php echo $entete; ?>">
	</div>

 </form>
 </div>
 
 <div style="display: block;float:right;width:40%">
	Prise de notes libre :<br />
	<textarea rows="30" cols="50" form="formform" name="notes"><?php echo $_POST["notes"]; ?></textarea>
	
	<form method="post" action="abandon_alea.php">
		<input type="submit" value="Je donne ma langue au chat">
		<div style="visibility: hidden">
			<input name="entete" value="<?php echo $entete; ?>">
			<input name="notes" value="<?php echo $_POST["notes"]; ?>">
		</div>
	</form>
</div>

</body></html>