<?php
session_start()
?>

<!DOCTYPE html>
<html lang="fr">
<head>
<meta charset="UTF-8">
<title>Bouche cousue : le jeu</title>
<link rel="shortcut icon" type="image/x-icon" href="favicon.ico" />
</head>
<body>

<div style="display: block;float:right;width:40%">
	Prise de notes libre :<br />
	<textarea rows="30" cols="50" form="formform" name="notes"><?php echo $_POST["notes"]; ?></textarea>
</div>

<div style="display: block;width:50%;float:left">
<?php
	$mot=$_SESSION["mot"];
	$entete=$_POST["entete"];
	echo $entete."<br /><br />Le mot à trouver était <b>$mot</b>.<br /><a href='https://fr.wiktionary.org/wiki/fronde' target='_blank'>Voir la page Wiktionnaire du mot</a><br />";
	echo "<br /><div style='text-align:center;'>Jouer avec un mot aléatoire ?</div>";
	include('bouchecousue2bis_nbLettres.php');
	
	$nom=$_POST["nom"];
	$add="$nom abandon, ";
	$data = fopen("data.txt","r+"); // On ouvre le fichier en lecture/écriture
	fseek($data,0,SEEK_END);		// On se place à la fin du fichier
	fputs($data,$add);     			// On écrit dans le fichier
	fclose($data);                  // On ferme le fichier
?>
</div>