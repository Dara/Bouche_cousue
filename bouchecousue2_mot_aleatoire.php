<?php
session_start()
?>

<!DOCTYPE html>
<html lang="fr">
<head>
<meta charset="UTF-8">
<title>Bouche cousue : le jeu</title>
<link rel="shortcut icon" type="image/x-icon" href="favicon.ico" />
</head>
<body>

<div style="display: block;width:50%;float:left">
<?php 
// Récupère le nombre de lettres
$longueur=$_POST['nbLettres'];
$s="mots_".$longueur."_lettres.txt";
$handle=fopen($s,"r");

// Calcule la longueur de la liste de mots
$compteur=-1;
while(!feof($handle)){
	$osef=fgets($handle);
	$compteur++;
}

fseek($handle,0);

// Trouve un mot au hasard
$hasard=mt_rand(1,$compteur);

for($i=0;$i<$hasard-1;$i++){
	$osef=fgets($handle);
}

$mot=fgetc($handle);
for($i=1;$i<$longueur;$i++){
	$mot=$mot.fgetc($handle);
}

fclose($handle);

$_SESSION['mot']=strtolower($mot);

//Affiche le 1er écran
$entete="<div style='border:solid;'><div style='text-align:center;'><a href='https://fr.wiktionary.org/wiki/Utilisateur:DaraDaraDara/bouchecousue' target='_blank'>Règles détaillées et exemple de partie</a></div>";
$entete=$entete."Rapide rappel des règles :<br />Pour chaque proposition, le nombre de lettres en commun (même lettre à la même position dans le mot) avec le mot à trouver sera affiché.</div><br />";
$entete=$entete."Tu dois trouver un mot de $longueur lettres. Bonne chance !";
echo $entete;

$_SESSION['coups']=0;
?> 

<form method="post" action="bouchecousue3_alea.php" id="formform">

	<input type="text" name="proposition" maxlength="<?php echo $longueur; ?>" size="<?php echo $longueur; ?>" autocomplete="off" autofocus>
	<input type="submit" />

	<div style="visibility: hidden">
		<input name="entete" value="<?php echo $entete; ?>">
	</div>

</form>
</div>

<div style="display: block;float:right;width:40%">
	Prise de notes libre :<br />
	<textarea rows="30" cols="50" form="formform" name="notes"></textarea>
</div>