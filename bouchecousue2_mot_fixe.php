<?php
session_start()
?>

<!DOCTYPE html>
<html lang="fr">
<head>
<meta charset="UTF-8">
<title>Bouche cousue : le jeu</title>
<link rel="shortcut icon" type="image/x-icon" href="favicon.ico" />
</head>
<body>

<div style="display: block;width:50%;float:left">
<?php
// récupère les variables de bouchecousue2_demande_nom
$nom=$_POST["nom"];
$entete=$_POST["entete"];

//Mise en place du mot --------------------------------------------- A MODIFIER
$mot="nareux";
$longueur=strlen($mot);
$_SESSION['mot']=$mot;

// initialise le nombre de coups
$_SESSION['coups']=0;

// MàJ affichage
$entete=$entete.$nom.", tu dois trouver un mot de $longueur lettres. Bonne chance !";
echo $entete;

?>

<form method="post" action="bouchecousue3_fixe.php" id="formform">

	<input type="text" name="proposition" maxlength="<?php echo $longueur; ?>" size="<?php echo $longueur; ?>" autocomplete="off" autofocus>
	<input type="submit" />

	<div style="visibility: hidden">
		<input name="entete" value="<?php echo $entete; ?>">
		<input name="nom" value="<?php echo $nom; ?>">
	</div>

</form>
</div>

<div style="display: block;float:right;width:40%">
	Prise de notes libre :<br />
	<textarea rows="30" cols="50" form="formform" name="notes"></textarea>
</div>

</body></html>