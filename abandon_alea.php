<?php
session_start()
?>

<!DOCTYPE html>
<html lang="fr">
<head>
<meta charset="UTF-8">
<title>Bouche cousue : le jeu</title>
<link rel="shortcut icon" type="image/x-icon" href="favicon.ico" />
</head>
<body>

<div style="display: block;float:right;width:40%">
	Prise de notes libre :<br />
	<textarea rows="30" cols="50" form="formform" name="notes"><?php echo $_POST["notes"]; ?></textarea>
</div>

<div style="display: block;width:50%;float:left">
<?php
	$mot=$_SESSION["mot"];
	$entete=$_POST["entete"];
	echo $entete."<br /><br />Le mot à trouver était <b>$mot</b>.<br /><a href='https://fr.wiktionary.org/w/index.php?title=Spécial:Recherche&search=$mot' target='_blank'>Voir la page Wiktionnaire du mot</a>";
	echo "<div style='text-align:center;'>Rejouer ?</div>";
	include('bouchecousue2bis_nbLettres.php');
?>
</div>